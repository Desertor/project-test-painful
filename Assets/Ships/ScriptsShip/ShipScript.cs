﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipScript : MonoBehaviour
{
    [Header("Ship Script Variables")]
    public Rigidbody2D rbShip;
    public Animator animatorShip;
    public float movementSpeedShip;
    public GameObject shootForwardPlayerSpawn;
    public Vector2 playerMovementDirection;
    public GameObject CannonBullet;
    public int lifeMax;
    public int lifeAmmount;
    public bool isAlive;
    public SpriteRenderer lifeRenderer;
    public GameSessionControllerScript gameSessionController;
    protected void ShootForward()
    {
        GameObject cannonBall = Instantiate(CannonBullet, shootForwardPlayerSpawn.transform.position, shootForwardPlayerSpawn.transform.rotation);
        cannonBall.GetComponent<CannonBallScript>().emittinTag = gameObject.tag.ToString();
    }

    protected void TakeDamage()
    {

        if (lifeAmmount - 1 >= 0)
        {
            lifeAmmount--;
            LifeUpdateUI();
            if (lifeAmmount == 0)
            {
                isAlive = false;
                
            }
        }

    }
    void LifeUpdateUI()
    {
        float lifePercentual = lifeAmmount / (float)lifeMax;
        lifeRenderer.transform.localScale = new Vector3(lifePercentual, lifeRenderer.transform.localScale.y, lifeRenderer.transform.localScale.z);
        int PercentualLifeAround = Mathf.FloorToInt(lifePercentual * 100);
        animatorShip.SetInteger("PercentualLifeLimit", PercentualLifeAround);
    }

    public void DestroyShip()
    {
        Destroy(this.gameObject);
    }
}
