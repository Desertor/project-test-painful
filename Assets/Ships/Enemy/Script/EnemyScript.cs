﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : ShipScript
{
    [Header("Enemy Script Variables")]
    public Transform playerTarget;
    public enum TypeShipEnum { Chaser,Shooter}
    public TypeShipEnum typeShip;
    public float timeFrequencyShooterTime;
    public float timeFrequencyShooterCounter;
    // Start is called before the first frame update
    void Start()
    {
        timeFrequencyShooterCounter = timeFrequencyShooterTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive && gameSessionController.IsPlay())
        {
            Vector3 direction = playerTarget.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 270;
            rbShip.rotation = angle;
            
        }

    }
    void FixedUpdate()
    {
        if (isAlive && gameSessionController.IsPlay())
        {
            if (typeShip == TypeShipEnum.Shooter)
            {
                
                if (Vector3.Distance(playerTarget.transform.position, this.transform.position) > 4f)
                {
                    if(Vector3.Distance(playerTarget.transform.position, this.transform.position) < 5f)
                    {
                        timeFrequencyShooterCounter -= Time.fixedDeltaTime;
                        if (timeFrequencyShooterCounter <= 0)
                        {
                            ShootForward();
                            timeFrequencyShooterCounter = timeFrequencyShooterTime;
                        }
                    }

                    rbShip.velocity = transform.up * movementSpeedShip;
                    rbShip.constraints = RigidbodyConstraints2D.FreezeRotation;
                }
                else
                {
                    timeFrequencyShooterCounter -= Time.fixedDeltaTime;
                    if (timeFrequencyShooterCounter <= 0)
                    {
                        ShootForward();
                        timeFrequencyShooterCounter = timeFrequencyShooterTime;
                    }
                    rbShip.velocity = Vector2.zero;
                    rbShip.constraints = RigidbodyConstraints2D.FreezeAll;
                }
            }
            else
            {
                rbShip.velocity = transform.up * movementSpeedShip;
                rbShip.constraints = RigidbodyConstraints2D.FreezeRotation;
            }

        }
        else
        {
            rbShip.velocity = Vector2.zero;
            rbShip.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        

    }
    
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CannonBallTag") && collision.gameObject.GetComponent<CannonBallScript>().emittinTag=="Player")
        {
            if (isAlive)
            {
                TakeDamage();
                if (lifeAmmount == 0)
                {
                    gameSessionController.GainScorePoint();
                    this.GetComponent<BoxCollider2D>().enabled = false;
                }
            }
            collision.gameObject.GetComponent<CannonBallScript>().collisionShip();


        }
    }

    public bool IsChaserType()
    {
        if (typeShip == TypeShipEnum.Chaser)
        {
            return true;
        }
        return false;
    }

    public void HitChaserShip()
    {
        TakeDamage();
    }

    public void ConfigEnemyShip()
    {
        int randomNumberType = Random.Range(0, 2);
        if (randomNumberType == 0){
            typeShip = TypeShipEnum.Shooter;
        }
        else
        {
            typeShip = TypeShipEnum.Chaser;
        }

        if (typeShip == TypeShipEnum.Shooter)
        {
            lifeMax = 5;
            animatorShip.SetLayerWeight(animatorShip.GetLayerIndex("Chaser Layer"), 0);
        }
        else
        {
            animatorShip.SetLayerWeight(animatorShip.GetLayerIndex("Chaser Layer"), 1);
            lifeMax = 1;
        }
        lifeAmmount = lifeMax;
        isAlive = true;
        playerTarget = GameObject.Find("Player'sShip").transform;
        gameSessionController = GameObject.Find("Main Camera").GetComponent<GameSessionControllerScript>();
    }
}
