﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : ShipScript
{
    [Header("Player Script Variables")]
    public float movementRotationShip;
    public GameObject[] shootSidePlayerSpawns;
    public Vector2 screenBounds;
    public float objectHeight;

    // Start is called before the first frame update
    void Start()
    {
        gameSessionController = GameObject.Find("Main Camera").GetComponent<GameSessionControllerScript>();
        lifeAmmount = lifeMax;
        isAlive = true;
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y / 2;

    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive && gameSessionController.IsPlay())
        {
            PlayerMovement();
            RotateMovement();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                ShootSide();
            }
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                ShootForward();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                TakeDamage();
            }
        }

    }

    void PlayerMovement()
    {
        playerMovementDirection.x = Input.GetAxisRaw("Horizontal");
        playerMovementDirection.y = Input.GetAxisRaw("Vertical");
    }

    void RotateMovement()
    {
        float rotation = playerMovementDirection.x * movementRotationShip;
        transform.Rotate(Vector3.back * rotation);
    }
    void FixedUpdate()
    {
        if(isAlive && gameSessionController.IsPlay())
        {
            rbShip.velocity = transform.up * Mathf.Clamp01(playerMovementDirection.y) * movementSpeedShip;
        }
        else
        {
            rbShip.velocity = Vector2.zero;
        }
        

    }

    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        if(viewPos.x< screenBounds.x * -1+ objectHeight)
        {
            viewPos.x = screenBounds.x * -1 + objectHeight;
            transform.position = viewPos;
        }
        else
        {
            if (viewPos.x > screenBounds.x- objectHeight)
            {
                viewPos.x = screenBounds.x - objectHeight;
                transform.position = viewPos;
            }
        }
        if (viewPos.y < screenBounds.y * -1 + objectHeight)
        {
            viewPos.y = screenBounds.y * -1 + objectHeight;
            transform.position = viewPos;
        }
        else
        {
            if (viewPos.y > screenBounds.y- objectHeight)
            {
                viewPos.y= screenBounds.y-objectHeight;
                transform.position = viewPos;
            }
        }
    }


    void ShootSide()
    {
        for(int i=0;i< shootSidePlayerSpawns.Length; i++)
        {
            GameObject cannonBall=Instantiate(CannonBullet, shootSidePlayerSpawns[i].transform.position, shootSidePlayerSpawns[i].transform.rotation);
            cannonBall.GetComponent<CannonBallScript>().emittinTag = gameObject.tag.ToString();
        }
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("EnemyTag"))
        {
            if (collision.gameObject.GetComponent<EnemyScript>().IsChaserType() && collision.gameObject.GetComponent<EnemyScript>().isAlive)
            {
                TakeDamage();
                if (lifeAmmount == 0)
                {
                    gameSessionController.GameOver();
                }
                collision.gameObject.GetComponent<EnemyScript>().HitChaserShip();
            }
        }


        
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CannonBallTag") && collision.gameObject.GetComponent<CannonBallScript>().emittinTag == "EnemyTag")
        {
            if (isAlive)
            {
                TakeDamage();
                
            }
            collision.gameObject.GetComponent<CannonBallScript>().collisionShip();

        }
    }
}
