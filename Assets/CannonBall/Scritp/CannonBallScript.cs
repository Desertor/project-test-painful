﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallScript : MonoBehaviour
{
    public string emittinTag;
    public bool isCollision;
    public Rigidbody2D rbCannonBall;
    public float movementSpeedCannon;
    public float timeDestroyBall;

    public Vector2 screenBounds;
    public float objectHeight;
    public Animator animatorCannonBall;
    public GameSessionControllerScript gameSessionController;
    private void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y / 2;
        gameSessionController = GameObject.Find("Main Camera").GetComponent<GameSessionControllerScript>();
        isCollision = false;
    }
    void FixedUpdate()
    {
        if (!isCollision && gameSessionController.IsPlay())
        {
            rbCannonBall.velocity = transform.up * movementSpeedCannon;
        }
        else
        {
            rbCannonBall.velocity = Vector2.zero;
        }
        
    }

    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        if (viewPos.x < screenBounds.x * -1 - objectHeight)
        {
            Destroy(this.gameObject);
        }
        else
        {
            if (viewPos.x > screenBounds.x + objectHeight)
            {
                Destroy(this.gameObject);
            }
        }
        if (viewPos.y < screenBounds.y * -1 - objectHeight)
        {
            Destroy(this.gameObject);
        }
        else
        {
            if (viewPos.y > screenBounds.y + objectHeight)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public void collisionShip()
    {
        isCollision = true; 
        animatorCannonBall.SetBool("Explode",true);
    }

    public void DestroyObject()
    {
        Destroy(this.gameObject);
    }

}
