﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
public class GameSessionControllerScript : MonoBehaviour
{
    public PlayerScript playerInfo;
    public GameCanvasUI canvasUIInfo;
    public float timeSecondsSession;
    public float timeSecondsEnemySpawnTotal;
    public float timeSecondsEnemySpawnCounter;
    public enum GameStatusEnum { Playing,GameOver}
    public GameStatusEnum gameStatus;
    public Vector2 screenBounds;
    public Collider2D[] collidersObject;
    public Tilemap mapIsland;
    public GameObject islandColliderSpawnGameObject;
    public GameObject islandColliderSPawnParent;
    public LayerMask layerpieceIsland;
    public GameObject enemyGameObject;
    public int maxEnemyAmmountSpawn;

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        timeSecondsSession = ManagerGame.Instance.gameSessionTime;
        timeSecondsEnemySpawnTotal = ManagerGame.Instance.enemySessionTime;
        timeSecondsEnemySpawnCounter = timeSecondsEnemySpawnTotal;
        canvasUIInfo.UpdateGameTimeUI(Mathf.CeilToInt(timeSecondsSession));
        canvasUIInfo.UpdateGameScoreUI(ManagerGame.Instance.playerScore);
        TileMapInformation();
        int spawnEnemiesAmount = Random.Range(1, maxEnemyAmmountSpawn+1);
        for (int i = 0; i < spawnEnemiesAmount; i++)
        {
            SpawnEnemyShip();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStatus == GameStatusEnum.Playing)
        {
            float timeDelta = Time.deltaTime;
            timeSecondsSession -= timeDelta;
            canvasUIInfo.UpdateGameTimeUI(Mathf.CeilToInt(timeSecondsSession));
            if (timeSecondsSession <= 0)
            {
                canvasUIInfo.UpdateGameTimeUI(0);
                GameOver();
            }
            if (!playerInfo.isAlive)
            {
                GameOver();
            }

            timeSecondsEnemySpawnCounter -= timeDelta;
            if (timeSecondsEnemySpawnCounter <= 0)
            {
                int spawnEnemiesAmount = Random.Range(1, maxEnemyAmmountSpawn+1);
                for (int i = 0; i < spawnEnemiesAmount; i++)
                {
                    SpawnEnemyShip();
                }
                timeSecondsEnemySpawnCounter = timeSecondsEnemySpawnTotal;
            }
        }

    }
    public bool IsPlay()
    {
        if (gameStatus == GameStatusEnum.Playing)
        {
            return true;
        }
        return false;
    }
    public void GameOver()
    {
        gameStatus = GameStatusEnum.GameOver;
        canvasUIInfo.ActiveGameOverUI(ManagerGame.Instance.playerScore);
    }
    public void GainScorePoint()
    {
        ManagerGame.Instance.playerScore++;
        canvasUIInfo.UpdateGameScoreUI(ManagerGame.Instance.playerScore);
    }
    public void SpawnEnemyShip()
    {
        bool canSpawnHere = false;
        Vector3 spawnPos = Vector3.zero;
        while (!canSpawnHere)
        {
            float spawnPosX = Random.Range(-screenBounds.x, screenBounds.x);
            float spawnPosY = Random.Range(-screenBounds.y, screenBounds.y);
            spawnPos = new Vector3(spawnPosX, spawnPosY, 0);
            canSpawnHere = PreventSpawnOverLap(spawnPos, enemyGameObject.GetComponent<SpriteRenderer>().sprite);
        }

        GameObject enemy = Instantiate(enemyGameObject, spawnPos, Quaternion.identity);
        enemy.GetComponent<EnemyScript>().ConfigEnemyShip();
    }
    bool PreventSpawnOverLap(Vector3 spawnPos, Sprite enemyShip)
    {
        
        collidersObject = Physics2D.OverlapCircleAll(Vector2.zero, screenBounds.x, layerpieceIsland);
        for(int i=0;i< collidersObject.Length; i++)
        {

            if (collidersObject[i].gameObject.layer == 8)
            {
                Vector3 centerCollider = collidersObject[i].bounds.center;
                float width = collidersObject[i].bounds.extents.x;
                float height = collidersObject[i].bounds.extents.y;

                float leftExtents = centerCollider.x - width - (enemyShip.bounds.size.x/2);
                float rightExtents = centerCollider.x + width + (enemyShip.bounds.size.x/2);
                float lowerExtents = centerCollider.y - height - (enemyShip.bounds.size.y/2);
                float upperExtents = centerCollider.y + height + (enemyShip.bounds.size.y/2);

                if(spawnPos.x>= leftExtents && spawnPos.x <= rightExtents)
                {
                    if (spawnPos.y >= lowerExtents && spawnPos.y <= upperExtents)
                    {
                        return false;
                    }
                }
                 
            }

        }
        return true;
        
    }

    void TileMapInformation()
    {
        Vector2 leftUpperLimit = new Vector2(-screenBounds.x, screenBounds.y);
        Vector2 rightBottonLimit = new Vector2(screenBounds.x, -screenBounds.y);
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int gridPositionLU = mapIsland.WorldToCell(leftUpperLimit);
        Vector3Int gridPositionRB = mapIsland.WorldToCell(rightBottonLimit);
        TileBase tileLU = mapIsland.GetTile(gridPositionLU);
        TileBase tileRB = mapIsland.GetTile(gridPositionRB);
        for(int i = gridPositionLU.x;i< gridPositionRB.x; i++)
        {
            for (int j = gridPositionRB.y; j < gridPositionLU.y; j++)
            {
                Vector3Int gridPosition = new Vector3Int (i, j, 0);
                TileBase tile = mapIsland.GetTile(gridPosition);
                if (tile != null)
                {
                    
                    Vector3 tilePosition = mapIsland.GetCellCenterWorld(gridPosition);

                    GameObject islandPiece = Instantiate(islandColliderSpawnGameObject, tilePosition, Quaternion.identity);
                    islandPiece.transform.SetParent(islandColliderSPawnParent.transform);
                }
               

            }
        }
    }
}
