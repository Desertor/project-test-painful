﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameCanvasUI : MonoBehaviour
{
    public Text gameTimeText;
    public Text gameScoreText;
    public Text gameScoreGameOverText;
    public GameObject gamePlayUI;
    public GameObject gameOverUI;
    // Start is called before the first frame update
    public void UpdateGameTimeUI(int secondsLeft)
    {
        gameTimeText.text = "" + secondsLeft;
    }
    public void UpdateGameScoreUI(int newScore)
    {
        gameScoreText.text = "SCORE: " + newScore;
    }

    public void ActiveGameOverUI(int finalScore)
    {
        gamePlayUI.SetActive(false);
        gameOverUI.SetActive(true);
        gameScoreGameOverText.text = "SCORE: " + finalScore;
    }

    public void PlayAgainAction()
    {
        ManagerGame.Instance.playerScore = 0;
        SceneManager.LoadScene("GameScene");
    }

    public void MenuAction()
    {
        ManagerGame.Instance.playerScore = 0;
        SceneManager.LoadScene("MenuScene");
    }
}
