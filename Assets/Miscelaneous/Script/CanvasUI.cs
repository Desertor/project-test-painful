﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CanvasUI : MonoBehaviour
{
    public GameObject menuUI;
    public GameObject optionsUI;
    
    
    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void Options()
    {
        menuUI.SetActive(false);
        optionsUI.SetActive(true);
    }

    public void BackMenu()
    {
        menuUI.SetActive(true);
        optionsUI.SetActive(false);
    }
}
