﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OptionsUI : MonoBehaviour
{
    public Slider sliderGameSessionTime;
    public Text textGameSessionTimeValue;
    public float minValueSecondsGameSession;
    public float maxValueSecondsGameSession;

    public Slider sliderEnemySpawnTime;
    public Text textEnemySpawnTimeValue;
    public float minValueSecondsEnemySpawn;
    public float maxValueSecondsEnemySpawn;
    // Start is called before the first frame update
    void Start()
    {
        minValueSecondsGameSession = ManagerGame.Instance.minValueSecondsGameSession;
        maxValueSecondsGameSession = ManagerGame.Instance.maxValueSecondsGameSession;
        minValueSecondsEnemySpawn = ManagerGame.Instance.minValueSecondsEnemySpawn;
        maxValueSecondsEnemySpawn = ManagerGame.Instance.maxValueSecondsEnemySpawn;
        TimeGameSession();
        TimeEnemySpawn();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void TimeGameSession()
    {
        int timeGameSession = Mathf.FloorToInt(minValueSecondsGameSession + (maxValueSecondsGameSession - minValueSecondsGameSession) * sliderGameSessionTime.value);
        textGameSessionTimeValue.text = ""+timeGameSession+" seconds";
        ManagerGame.Instance.TimeGameSession(timeGameSession);
    }

    public void TimeEnemySpawn()
    {
        int timeEnemySpawn = Mathf.FloorToInt(minValueSecondsEnemySpawn + (maxValueSecondsEnemySpawn - minValueSecondsEnemySpawn) * sliderEnemySpawnTime.value);
        textEnemySpawnTimeValue.text = "" + timeEnemySpawn+" seconds";
        ManagerGame.Instance.TimeEnemySpawn(timeEnemySpawn);
    }
}
