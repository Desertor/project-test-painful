﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGame : MonoBehaviour
{
    public static ManagerGame Instance { get; private set; }
    [Header("Game Session")]
    public int gameSessionTime;
    public int enemySessionTime;
    public int playerScore;
    [Header("Game Config")]
    public float minValueSecondsGameSession;
    public float maxValueSecondsGameSession;
    public float minValueSecondsEnemySpawn;
    public float maxValueSecondsEnemySpawn;
    private void Awake()
    {
        if (Instance == null)
        {
            gameSessionTime = (int)minValueSecondsGameSession;
            enemySessionTime = (int)minValueSecondsEnemySpawn;
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void TimeGameSession (int time)
    {
        gameSessionTime = time;
    }
    public void TimeEnemySpawn(int time)
    {
        enemySessionTime = time;
    }
}
